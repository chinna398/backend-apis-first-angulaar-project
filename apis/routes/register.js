const express = require('express');
const multer = require('multer');
const mongoose = require('mongoose');
const mongoClient = require('mongodb').MongoClient;
const bcrypt = require('bcrypt');
const router = express.Router();
const User = require('../models/user');
const url = 'mongodb://localhost:27017/';

const storage = multer.diskStorage({
    destination:function(req,file,cb){
        console.log("file destination===========");
        cb(null,'./uploads/');
    },
    filename:function(req,file,cb){
        console.log('file name:',new Date().toISOString()+file.originalname);
        cb(null,new Date().toISOString()+file.originalname);
    }
});

const fileFilter = (req,file,cb)=>{
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png')
    {
        console.log('file type:',file.mimetype);
        cb(null,true);
    }
    else{
        console.log('file type is not correct');
        cb(null,false);
    }
}

const upload = multer({
    storage:storage,
    fileFilter:fileFilter
    
})

router.get('/' , (req,res) => {
    res.status(200).json({
        message: 'Handling GET request for /registration page'
    });
});

router.post('/' ,(req,res,next) =>{
    mongoClient.connect(url,(err,db)=>{
        if(err){
            console.log("mongodb connection failed\n",err);
            res.status(500).json({
                error: err
            });
        }else{
            console.log('connection succesful\n');
            const dbo = db.db('college');
            dbo.collection('students').findOne({email:req.body.email})
            .then(user =>{
                if(user)
                {
                    console.log('User already exist\n');
                    return res.status(201).json({
                        message:'User already exist'
                    });
                }else{
                    bcrypt.hash(req.body.password,10,(err,hash) =>{
                        if(err){
                            console.log('error at password hashing\n',err);
                            return res.status(500).json({
                                error:err
                            });
                        }
                        else{
                            const userdata = new User({
                                _id: new mongoose.Types.ObjectId(),
                                username: req.body.name,
                                email: req.body.email,
                                password: hash,
                            });
                            dbo.collection('students').insertOne(userdata,(err,result) =>{
                                if(err){
                                    console.log('error at inserting data\n',err);
                                    return res.status(500).json({
                                        message:'Error at inserting data'
                                    })
                                }else{
                                    console.log('Successfully user created\n',result);
                                    res.status(201).json({
                                        message:'Successfully user created'
                                    })
                                }
                            })
                            db.close();
                        }
                    })
                }      
            })
            .catch(err =>{
                console.log(err);
                res.status(500).json({
                    error:err
                })
            })
        }
    });
});
module.exports = router;