const express = require('express');
const mongoCLient = require('mongodb').mongoCLient;
const url = 'mongodb://localhost:27017';

const router = express.Router();

//Process with login page
router.post('/',(req,res) =>{
    mongoCLient.connect(url, (err, db) =>{
        const dbo = db.db('college');
        if(err){
            console.log('Database connection failed\n',err);
            res.status(500).json({
                message:'database connection failed\n'
            })
        }
        else
        {
            dbo.collection('students').findOne({ email:req.body.email})
            .then(user =>{
                console('successfully user found\n',user);
                return res.status(201).json({
                    message: 'successfully user found'
                })
            })
            .catch(err =>{
                console.log('user id not found\n',err);
            })
            console.log('Database conection success\n');
            db.close();
        }
    });
});
module.exports = router;
