const express = require('express');

const router = express.Router();

router.get('/' , (req,res) => {
    res.status(200).json({
        message: 'Handling GET request for /registration page'
    });
});
module.exports = router;