const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');

const regiRoutes = require('./apis/routes/register');
const logRoutes = require('./apis/routes/login');

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(express.static('uploads'));

app.use((req,res,next) =>{
    res.header('Access-Control-Allow-Origin','*');
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if(req.method === 'OPTIONS'){
        res.header('Access-Control-Allow-Method','PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json();
    }
    next();
});

app.use('/saveUserData',regiRoutes);
app.use('/userLogin',logRoutes);

//error handling
app.use((req,res,next) =>{
    const error = new Error("Not Found");
    error.status = 400;
    next(error);
});

//another way of error handling
app.use((error,req,res,next) =>{
    res.status(error.status || 500);
    res.json({
        error:{
            message:error.message
        }
    })
});

module.exports = app;